﻿DROP DATABASE IF EXISTS test;
CREATE DATABASE test;

USE test;

CREATE TABLE preguntas(
  id int AUTO_INCREMENT,
  enunciado varchar(200),
  PRIMARY KEY(id)
);

CREATE TABLE respuestas (
  id int AUTO_INCREMENT,
  enunciado varchar(200),
  PRIMARY KEY(id)

);